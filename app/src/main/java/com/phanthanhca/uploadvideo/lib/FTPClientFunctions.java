package com.phanthanhca.uploadvideo.lib;

import android.content.Context;
import android.util.Log;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Created by phanthanhca on 15/07/2017.
 */

public class FTPClientFunctions {

    private static final String TAG = "FTPClientFunctions";
    public FTPClient mFTPClient = null;

    /* todo : Method to connect to FTP server */
    public boolean ftpConnect(String host, String username, String password, int port) {
        try {
            mFTPClient = new FTPClient();
            // connecting to the host
            mFTPClient.connect(host, port);

            if (FTPReply.isPositiveCompletion(mFTPClient.getReplyCode())) {

                boolean status = mFTPClient.login(username, password);
                mFTPClient.setFileType(FTP.BINARY_FILE_TYPE);
                mFTPClient.enterLocalPassiveMode();

                return status;
            }
        } catch (Exception e) {

            Log.e(TAG, "Error: could not connect to host " + host);
        }

        return false;
    }


     /* todo : Method to disconnect from FTP server */

    public boolean ftpDisconnect() {
        try {
            mFTPClient.logout();
            mFTPClient.disconnect();
            return true;

        } catch (Exception e) {
            Log.d(TAG, "Error occurred while disconnecting from ftp server.");
        }

        return false;
    }


    /* todo: Method to get current working directory */

    public String ftpGetCurrentWorkingDirectory() {
        try {
            String workingDir = mFTPClient.printWorkingDirectory();
            return workingDir;
        } catch (Exception e) {
            Log.e(TAG, "Error: could not get current working directory.");
        }

        return null;
    }


     /* todo : Method to change working directory */

    public boolean ftpChangeDirectory(String directory_path) {
        try {
            mFTPClient.changeWorkingDirectory(directory_path);
        } catch (Exception e) {
            Log.e(TAG, "Error: could not change directory to " + directory_path);
        }

        return false;
    }


    /* todo : Method to list all files in a directory */

    public String[] ftpPrintFilesList(String dir_path) {
        String[] fileList = null;
        try {
            FTPFile[] ftpFiles = mFTPClient.listFiles(dir_path);
            int length = ftpFiles.length;
            fileList = new String[length];
            for (int i = 0; i < length; i++) {
                String name = ftpFiles[i].getName();
                boolean isFile = ftpFiles[i].isFile();

                if (isFile) {
                    fileList[i] = "File :: " + name;
                    Log.e(TAG, "File : " + name);
                } else {
                    fileList[i] = "Directory :: " + name;
                    Log.e(TAG, "Directory : " + name);
                }
            }
            return fileList;

        } catch (Exception e) {
            e.printStackTrace();
            return fileList;
        }
    }


    /* todo : Method to create new directory */

    public boolean ftpMakeDirectory(String new_dir_path) {
        try {
            boolean status = mFTPClient.makeDirectory(new_dir_path);
            return status;

        } catch (Exception e) {
            Log.e(TAG, "Error: could not create new directory named " + new_dir_path);
        }

        return false;
    }


     /* todo: Method to delete/remove a directory */

    public boolean ftpRemoveDirectory(String dir_path) {
        try {
            boolean status = mFTPClient.removeDirectory(dir_path);
            return status;

        } catch (Exception e) {
            Log.e(TAG, "Error: could not remove directory named " + dir_path);
        }

        return false;
    }


    /* todo: Method to delete a file */

    public boolean ftpRemoveFile(String filePath) {
        try {
            boolean status = mFTPClient.deleteFile(filePath);
            return status;

        } catch (Exception e) {
            Log.e(TAG, "Error:" + e.getMessage());
        }

        return false;
    }


    /* todo : Method to rename a file */

    public boolean ftpRenameFile(String from, String to) {
        try {
            boolean status = mFTPClient.rename(from, to);
            return status;

        } catch (Exception e) {
            Log.e(TAG, "Could not rename file: " + from + " to: " + to);
        }

        return false;
    }


     /* todo : Method to download a file from FTP server */

    public boolean ftpDownload(String srcFilePath, String desFilePath) {
        boolean status = false;
        try {

            FileOutputStream desFileStream = new FileOutputStream(desFilePath);
            status = mFTPClient.retrieveFile(srcFilePath, desFileStream);
            desFileStream.close();

            return status;

        } catch (Exception e) {
            Log.e(TAG, "download failed");
        }

        return status;
    }


     /* todo : Method to upload a file to FTP server */

    public boolean ftpUpload(String srcFilePath, String desFileName, String desDirectory, Context context) {
        boolean status = false;
        try {

            FileInputStream srcFileStream = new FileInputStream(srcFilePath);
            Log.e(TAG, "FileName:" + desFileName);

            status = mFTPClient.storeFile("/domains/ddthuyhao.com/public_html/demovideo/" + desFileName, srcFileStream);
            srcFileStream.close();

            return status;

        } catch (Exception e) {
            Log.e(TAG, "upload failed: " + e);
        }

        return status;
    }
}
