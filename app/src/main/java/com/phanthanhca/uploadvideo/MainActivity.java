package com.phanthanhca.uploadvideo;

import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.ActionMenuItemView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.phanthanhca.uploadvideo.lib.FTPClientFunctions;
import com.phanthanhca.uploadvideo.util.Utilities;

public class MainActivity extends AppCompatActivity {

    public static final int FILE_SELECT_CODE = 101;
    private FTPClientFunctions ftpclient = null;
    private static final String TAG = "MainActivity";
    /* todo :  layout */
    private EditText etUsername;
    private EditText ethpohoneCuahang;
    private EditText edEmail;
    private CheckBox chbRule;
    private Button btnUploadVideo;
    private ProgressDialog pd;
    String FileName = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();

        /* todo : call init FTPClientFunctions */
        ftpclient = new FTPClientFunctions();
        connectToFTPAddress();
    }


    private void findViews() {

        etUsername = (EditText) findViewById(R.id.et_username);
        ethpohoneCuahang = (EditText) findViewById(R.id.ethpohone_cuahang);
        edEmail = (EditText) findViewById(R.id.ed_email);
        chbRule = (CheckBox) findViewById(R.id.chb_rule);
        btnUploadVideo = (Button) findViewById(R.id.btn_upload_video);

        /* todo : action choose file upload */
        btnUploadVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pd = ProgressDialog.show(MainActivity.this, "", "Uploading...", true, false);
                boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
                if (isKitKat) {
                    Intent intent = new Intent();
                    intent.setType("*/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(intent, FILE_SELECT_CODE);
                } else {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("*/*");
                    startActivityForResult(intent, FILE_SELECT_CODE);
                }
            }
        });
    }


    /* todo : connect server fpt */
    private void connectToFTPAddress() {

        final String host = "fpt.ddthuyhao.com";
        final String username = "ddthuyhao";
        final String password = "ca@123456";

        new Thread(new Runnable() {
            public void run() {
                boolean status = false;
                status = ftpclient.ftpConnect(host, username, password, 21);
                if (status == true) {
                    Log.e(TAG, "Connection Ftp Server Success ");

                } else {
                    Log.e(TAG, "Connection Ftp Server failed");

                }
            }
        }).start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == FILE_SELECT_CODE) {

               new Thread(new Runnable() {
                    public void run() {
                        try {

                            final Uri tempUri = data.getData();
                            boolean status = false;
                            String tempPath = Utilities.getFilePath(getBaseContext(), tempUri);
                            status = ftpclient.ftpUpload(tempPath, tempPath.substring(tempPath.lastIndexOf("/") + 1), "/", getBaseContext());
                            FileName= tempPath.substring(tempPath.lastIndexOf("/") + 1);
                            if (status == true) {
                                handler.sendEmptyMessage(2);
                            } else {
                                handler.sendEmptyMessage(-1);
                            }
                        } catch (Exception e) {

                        }

                    }
                }).start();
            }


        }
    }


    private Handler handler = new Handler() {

        public void handleMessage(android.os.Message msg) {
            if (pd != null && pd.isShowing()) {
                pd.dismiss();
            }
            if (msg.what == 2) {

                Toast.makeText(MainActivity.this, "Uploaded Successfully!",Toast.LENGTH_LONG).show();
                String temptoEmail ="mailto:" + edEmail.getText().toString();
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, FileName);
                intent.putExtra(Intent.EXTRA_TEXT, "http://ddthuyhao.com/demovideo/"+FileName);
                intent.setData(Uri.parse(temptoEmail));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            } else {
                Toast.makeText(MainActivity.this, "Uploaded Fail!",Toast.LENGTH_LONG).show();
            }

        }

    };

}
